package br.com.ifpe.oxefoodlucas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OxefoodLucasApplication {

	public static void main(String[] args) {
		SpringApplication.run(OxefoodLucasApplication.class, args);
	}

}
